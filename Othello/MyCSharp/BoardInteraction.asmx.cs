﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Othello.MyCSharp
{
    /// <summary>
    /// A webservice to provide the means to interact with the Othello board
    /// Manipulates the board string to manage turns and player choices
    /// </summary>
    /// 
    [WebService(Namespace = "https://tempuri.org")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    

    //Allow this to be called from script
    [System.Web.Script.Services.ScriptService]
    public class BoardInteraction : System.Web.Services.WebService
    {
        const int BOARD_DIM = 8;
        // Utility function for traversing the board ; if in == out then its invalid
        int indexUp(int idx)
        {
            return idx >= BOARD_DIM ? (idx - BOARD_DIM) : idx;
        }
        int indexDown(int idx)
        {
            return (idx < BOARD_DIM * (BOARD_DIM - 1)) ? (idx + BOARD_DIM) : idx; 
        }
        int indexLeft(int idx)
        {
            int onRow = idx % BOARD_DIM;
            return (onRow != 0) ? (idx - 1) : idx;
        }
        int indexRight(int idx)
        { 
            int onRow = idx % BOARD_DIM;
            return (onRow != (BOARD_DIM - 1)) ? (idx + 1) : idx; 
        }

        // Heavier util funcs for diagonals
        int indexUL(int idx)
        {
            // try up; if it doesn't change, back out early
            int idxU = indexUp(idx);
            if (idxU == idx)
            {
                return idx;
            }
            // then run left; ensure it moved left; otherwise back out
            int idxUL = indexLeft(idxU);
            if (idxUL == idxU)
            {
                return idx;
            }
            else
                return idxUL;
        }
        int indexDL(int idx)
        {
            // try up; if it doesn't change, back out early
            int idxD = indexDown(idx);
            if (idxD == idx)
            {
                return idx;
            }
            // then run left; ensure it moved left; otherwise back out
            int idxDL = indexLeft(idxD);
            if (idxDL == idxD)
            {
                return idx;
            }
            else
                return idxDL;
        }
        int indexUR(int idx)
        {
            // try up; if it doesn't change, back out early
            int idxU = indexUp(idx);
            if (idxU == idx)
            {
                return idx;
            }
            // then run right; ensure it moved right; otherwise back out
            int idxUR = indexRight(idxU);
            if (idxUR == idxU)
            {
                return idx;
            }
            else
                return idxUR;
        }
        int indexDR(int idx)
        {
            // try down; if it doesn't change, back out early
            int idxD = indexDown(idx);
            if (idxD == idx)
            {
                return idx;
            }
            // then run left; ensure it moved left; otherwise back out
            int idxDR = indexRight(idxD);
            if (idxDR == idxD)
            {
                return idx;
            }
            else
                return idxDR;
        }

        // define the delegate type for board traversal
        delegate int Mover(int idx);

        //Create the index of a space from its board coordinates
        int makeBoardIndex(int x, int y)
        {
            return x + (y * BOARD_DIM);
        }

        const char FREE_SPACE_CHAR = 'g';

        // Check if there is a valid run in a given direction
        // IN   :   board           : string representing the board
        //          idxAt           : the index of the board we would be placing the counter, and the starting point of the run
        //          placeTypeChar   : the character representing the token type to place
        //          moveFunc        : function used to traverse the board in a given direction
        // OUT  :   bool representing whether valid run in given direction
        bool IsValidRunInDirection(string board, int idxAt, char placeTypeChar, Mover moveFunc)
        {
            int lastCheckedIdx = idxAt;
            int thisCheckedIdx = moveFunc(idxAt);

            // if the move was succesful and the next in this direction is not a run ender
            if (thisCheckedIdx != idxAt && (board[thisCheckedIdx] != 'g') && (board[thisCheckedIdx] != placeTypeChar))
            {
                // continue moving left until we hit an end
                while (thisCheckedIdx != lastCheckedIdx)
                {
                    char thisSpace = board[thisCheckedIdx];
                    if (thisSpace == 'g')
                    {
                        // force end because g is an end
                        thisCheckedIdx = lastCheckedIdx;
                    }
                    else if (thisSpace == placeTypeChar)
                    {
                        // we have found an encapsulated run of other colours; this is a valis space and so we return true
                        return true;
                    }
                    else
                    {
                        // otherwise advance this sequence
                        lastCheckedIdx = thisCheckedIdx;
                        thisCheckedIdx = moveFunc(thisCheckedIdx);
                    }
                }
            }

            return false;
        }

        // Check if there is a valid placement for one colour in a space
        // IN   :   board           : string representing the board
        //          xAt             : x position to place this counter
        //          yAt             : y position to place this counter
        //          placeTypeChar   : the character representing the token type to place
        // OUT  :   bool representing whether there are valid placements from this position
        bool IsValidPlacement(string board, int xAt, int yAt, char placeTypeChar)
        {
            int idxAt = makeBoardIndex(xAt, yAt);       // find index of placement space on the board

            if (board[idxAt] != 'g')
            {
                return false;
            }
            if (IsValidRunInDirection(board, idxAt, placeTypeChar, indexLeft)
                || IsValidRunInDirection(board, idxAt, placeTypeChar, indexRight)
                || IsValidRunInDirection(board, idxAt, placeTypeChar, indexUp)
                || IsValidRunInDirection(board, idxAt, placeTypeChar, indexDown)
                || IsValidRunInDirection(board, idxAt, placeTypeChar, indexUL)
                || IsValidRunInDirection(board, idxAt, placeTypeChar, indexUR)
                || IsValidRunInDirection(board, idxAt, placeTypeChar, indexDL)
                || IsValidRunInDirection(board, idxAt, placeTypeChar, indexDR))
            {
                return true;
            }
            else
                return false;
        }

        // Resolve placing a counter of a given colour in one direction
        // IN   :   board           : char array representing the baord 
        //          idxAt           : the index to place at
        //          placeTypeChar   : the character representing the token type to place
        //          moveFunc        : function used to traverse the board in a given direction
        // OUT  :   char array representing the board
        char[] ResolvePlacementInDirection(char[] board, int idxAt, char placeTypeChar, Mover moveFunc)
        {
            if (IsValidRunInDirection(new string(board), idxAt, placeTypeChar, moveFunc))
            {
                int lastIdx = idxAt;
                int currIdx = moveFunc(idxAt);

                do
                {
                    board[currIdx] = placeTypeChar;
                    // move on
                    lastIdx = currIdx;
                    currIdx = moveFunc(currIdx);
                }
                while (board[currIdx] != placeTypeChar);
            }
            return board;
        }
        // Resolve placing a counter of a given colour in a space
        // IN   :   board           : string representing the baord 
        //          xAt             : x position to place this counter
        //          yAt             : y position to place this counter
        //          placeTypeChar   : the character representing the token type to place
        // OUT  :   string representing the board
        string ResolvePlacement(string board, int xAt, int yAt, char placeTypeChar)
        {
            int idxAt = makeBoardIndex(xAt, yAt);
            char[] charBoard = board.ToCharArray();
            charBoard[idxAt] = placeTypeChar;

            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexLeft);
            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexRight);
            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexUp);
            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexDown);
            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexUR);
            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexUL);
            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexDL);
            charBoard = ResolvePlacementInDirection(charBoard, idxAt, placeTypeChar, indexDR);


            return new string(charBoard);
        }

        
        [WebMethod]
        // Function to get the score for a given colour by counting chars in the board string
        // Returns the number of tokens of the given colour in a string format, e.g. "2"
        public string GetScoreForColour(string board, string tokenColour)
        {
            //turn string to char
            char toLookFor = tokenColour == "white" ? 'w' :
                tokenColour == "black" ? 'b' : 'x';

            int runningCount = 0;

            for (int y = 0; y < BOARD_DIM; ++y)
            {
                for (int x = 0; x < BOARD_DIM; ++x)
                {
                    // if the given space on the board has what we're looking for, increase the count
                    if (board[makeBoardIndex(x, y)] == toLookFor)
                        ++runningCount;
                }
            }

            //return the count
            return runningCount.ToString();
        }

        [WebMethod]
        // Function to resolve a token placement by one player
        // Has multiple return types that need to be handled
        //  - "invalid"         : this location was not a valid placement of the tile
        //  - otherwise this was a valid move and a string will be returned containing the new board
        public string ResolvePlayerTurn(string board, string placeX, string placeY, string tokenColor)
        {
            //turn string to char
            char toPlace = tokenColor == "black" ? 'b' :
                tokenColor == "white" ? 'w' : 'x';

            // if we can't place in this space, then return the invalid result
            if (!IsValidPlacement(board, Convert.ToInt32(placeX), Convert.ToInt32(placeY), toPlace))
            {
                return "invalid";
            }

            // otherwise return the board as string after resolving the placement
            board = ResolvePlacement(board, Convert.ToInt32(placeX), Convert.ToInt32(placeY), toPlace);

            return board;
        }


        [WebMethod]
        // Function to check if there are any moves open to one player
        // Returns either true or false, true if there are available moves, false otherwise
        // Result is in string format; "true" or "false"
        public string AreAnyMovesOpenToPlayer(string board, string tokenColor)
        {
            // turn string to char
            char toPlace = tokenColor == "black" ? 'b' :
                    tokenColor == "white" ? 'w' : 'x';

            // for each space on the board see if there are valid placements
            for (int y = 0; y < BOARD_DIM; ++y)
            {
                for (int x = 0; x < BOARD_DIM; ++x)
                {
                    //break flow and return true as soon as we find a valid placement
                    if (IsValidPlacement(board, x, y, toPlace))
                        return "true";
                }
            }

            return "false";
        }

    }
}
