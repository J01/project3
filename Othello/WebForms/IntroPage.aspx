﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IntroPage.aspx.cs" Inherits="Othello.WebForms.IntroPage" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%">

<head runat="server">
    <title>Othello Menu</title>
    <meta charset="utf-8" />
    <meta name="description" content="The menu page for Web Othello" />
    <meta name="keywords" content="othello,game,board,menu,intro" />
    <meta name="author" content="Jake Johnson Powell"/>
    <link rel="icon" type="image/x-icon" href="../othelloFavicon.ico" />

    <!-- Link to this page CSS-->
    <link href="~/MyCSS/IntroPageCSS.css" rel="stylesheet" media="screen" runat="server" type="text/css" />
</head>

<body style="">
    <form id="frmIntroPage" runat="server">
        
        <!-- page background -->
        <div id="backgroundImg">
        </div>

        <!-- Main div -->
        <div style="font-weight: 700;">
            <div style="height:18vh;">
            </div>

            <!-- Div for centering the title-->
            <div style="text-align:center;">            
                <asp:Label ID="lblTitle" runat="server" Text="Web Othello" Width ="20%"
                Font-Names="Garamond" Font-Size="32pt" CssClass="introTitle" BorderColor="Black" BorderStyle="None" Font-Overline="False" ForeColor="White" ></asp:Label>
            </div>    

            <div style="height:35vh;">
            </div>

            <!-- Div for centering the start button -->
            <div style="text-align:center;">
                <asp:Button ID="btnGoToGame" runat="server" Text="PLAY GAME" OnClick="btnGoToGame_Click" Width="20%" Height ="10%" CssClass="introButton" />
            </div>
            <br />
            <div style="text-align:center;">
                <asp:Button ID="btnGoToInstructions" runat="server" Text="INSTRUCTIONS" OnClick="btnGoToInstructions_Click" Width="20%" Height ="10%" CssClass="introButton" />
            </div>
        </div>

    </form>
</body>
</html>
