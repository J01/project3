﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Othello.WebForms
{
    public partial class IntroPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // nothing to do on page load so

        }

        protected void btnGoToGame_Click(object sender, EventArgs e)
        {
            // Just redirect the player to the main game page
            Response.Redirect("GamePage.aspx");
        }

        protected void btnGoToInstructions_Click(object sender, EventArgs e)
        {
            // Just redirect the player to the instruction page
            Response.Redirect("Instructions.aspx");
        }
    }
}