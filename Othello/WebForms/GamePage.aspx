﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GamePage.aspx.cs" Inherits="Othello.WebForms.GamePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">


<head runat="server">
    <title>Web Othello</title>
    <meta charset="utf-8" />
    <meta name="description" content="The game page for Web Othello" />
    <meta name="keywords" content="othello,game,board" />
    <meta name="author" content="Jake Johnson Powell"/>
    <link rel="icon" type="image/x-icon" href="../othelloFavicon.ico" />

    <!-- Link to JQuery for on page scripting -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Link to JQuery stylesheets -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"/>
    <!-- Link to JQuery UI scripts --> 
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- Link to page CSS -->
    <link href="../MyCSS/GamePageCSS.css" rel="stylesheet" type="text/css"/>
    
</head>

<body>

    <form id="form1" runat="server">
        <!-- Resources -->
        <!-- https://www.w3schools.com/xml/ajax_intro.asp -->


        <!-- In page script -->
        <script type="text/javascript">
            <!-- To represent the board we use a string -->
            const BOARD_WIDTH = 8;
            const BOARD_HEIGHT = 8;
            var boardContents = '';

            const BLACK_TILE_SRC = "../images/blackCounter.png";
            const WHITE_TILE_SRC = "../images/whiteCounter.png";

            const GREEN_SPACE = 'g';
            const WHITE_SPACE = 'w';
            const BLACK_SPACE = 'b';

            var whosGo = "white";
            var gameIsDone = false;

            // function to just start up the game
            function refreshBoardContents()
            {
                // create the board contents as a long char array, with the contents of spaces represented by the characer in that space
                boardContents =
                    `gggggggg\
gggggggg\
gggggggg\
gggbwggg\
gggwbggg\
gggggggg\
gggggggg\
gggggggg`;

                drawBoardContents();
            }
            window.onload = refreshBoardContents;


            // Update indicator of whos turn it is
            function updateTurnIndicator() {
                var indicatorDiv = document.getElementById("turnIndicator");

                if (indicatorDiv !== null) {
                    if (whosGo === "white") {
                        indicatorDiv.innerHTML = "It is white's go";
                    }
                    else if (whosGo === "black") {
                        indicatorDiv.innerHTML = "It is black's go";
                    }
                    else {
                        indicatorDiv.innerHTML = "It's someone's go, but who knows who eh?"
                    }
                }
                else {
                    alert("Could not locate turnIndicator");
                }
            }
            // Write specific scores
            function writeWhiteScore() {
                // make sure we can find the write location
                var table = document.getElementById("scoreTable");
                var cell = table.rows[0].cells[0];

                //call on ajax to get the white score
                var data_to_send = new Object();
                data_to_send.board = boardContents;
                data_to_send.tokenColour = "white";

                var json_data = JSON.stringify(data_to_send);
                $.ajax({
                    type: 'POST',
                    url: '../MyCSharp/BoardInteraction.asmx/GetScoreForColour',
                    data: json_data,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        cell.innerHTML = "White : " + data.d;
                    },
                    failure: function (data) {
                        alert("Could not retrieve white score");
                    }
                })
            }
            function writeBlackScore() {
                // make sure we can find the write location
                var table = document.getElementById("scoreTable");
                var cell = table.rows[0].cells[2];

                //call on ajax to get the white score
                var data_to_send = new Object();
                data_to_send.board = boardContents;
                data_to_send.tokenColour = "black";

                var json_data = JSON.stringify(data_to_send);
                $.ajax({
                    type: 'POST',
                    url: '../MyCSharp/BoardInteraction.asmx/GetScoreForColour',
                    data: json_data,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        cell.innerHTML = "Black : " + data.d;
                    },
                    failure: function (data) {
                        alert("Could not retrieve white score");
                    }
                })
            }
            // Function to get the scores of both players and write them to labels
            function updateScores()
            {
                writeWhiteScore();
                writeBlackScore();
            }
            // Function that draw the board contents to the corresponding divs
            function drawBoardContents()
            {
                var xCtr, yCtr;
                var boardTable = document.getElementById("board");

                if (boardTable !== null) {
                    // go through all spaces in the table and fill in with the appropriate image
                    for (yCtr = 0; yCtr < BOARD_HEIGHT; ++yCtr) {
                        for (xCtr = 0; xCtr < BOARD_WIDTH; ++xCtr) {

                            var thisSpaceObj = boardTable.rows[yCtr].cells[xCtr];                   // get this table element
                            var thisSpaceChar = boardContents.charAt(yCtr * BOARD_WIDTH + xCtr);    // get the letter in this space

                            // make sure that we are referencing a td with an image child
                            if (thisSpaceObj.hasChildNodes() && thisSpaceObj.childNodes[0].nodeName.toLowerCase() === 'img')
                            {
                                switch (thisSpaceChar) {
                                    case 'g':
                                        thisSpaceObj.childNodes[0].src = '..\\images\\transparentSpace.png';
                                        break;
                                    case 'b':
                                        thisSpaceObj.childNodes[0].src = BLACK_TILE_SRC;
                                        break;
                                    case 'w':
                                        thisSpaceObj.childNodes[0].src = WHITE_TILE_SRC;
                                        break;
                                    default:
                                        alert("Space contains invalid char data; game will not function correctly");
                                        break;
                                }
                            }
                            else
                            {
                                alert("Attempting to draw to a space with no image tag child ; graphics will be incomplete");
                            }
                        }
                    }
                }
                else
                {
                    alert("Cannot locate game board; game will not be available at this time");
                }

                updateScores();
                updateTurnIndicator();
            }


            // function to be activated when the return to menu button is clicked
            function returnToMenu() {
                // redirect to menu
                window.location.href = "../WebForms/IntroPage.aspx";
            }
            // Stage functions for game over
            function gameOverWriteData(whiteScore, blackScore) {
                // find the winner label and write who won
                var winnerLabel = document.getElementById("whoWinsText");
                if (winnerLabel === null) {
                    alert("Could not find winner label");
                    return;
                }

                if (whiteScore > blackScore) {
                    winnerLabel.innerHTML = "WHITE WINS!";
                }
                else if (blackScore > whiteScore) {
                    winnerLabel.innerHTML = "BLACK WINS!";
                }
                else {
                    winnerLabel.innerHTML = "DRAW";
                }

                // find the score table and write out the score
                var finalScoreTable = document.getElementById("finalScoreTable");
                if (finalScoreTable === null) {
                    alert("Could not find score table");
                }
                finalScoreTable.rows[1].cells[0].innerHTML = blackScore;
                finalScoreTable.rows[1].cells[1].innerHTML = whiteScore;
            }
            function gameOverRequestBlackScore(whiteScore) {
                // request the black score via AJAX
                var data_to_send = new Object();
                data_to_send.board = boardContents;
                data_to_send.tokenColour = "black";

                var json_data = JSON.stringify(data_to_send);
                $.ajax({
                    type: 'POST',
                    url: '../MyCSharp/BoardInteraction.asmx/GetScoreForColour',
                    data: json_data,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        var blackScore = data.d;
                        gameOverWriteData(whiteScore, blackScore);
                    },
                    failure: function (data) {
                        alert("Could not retrieve white score");
                    }
                })
            }
            function gameOverRequestWhiteScore() {
                // request the white score via AJAX
                var data_to_send = new Object();
                data_to_send.board = boardContents;
                data_to_send.tokenColour = "white";

                var json_data = JSON.stringify(data_to_send);
                $.ajax({
                    type: 'POST',
                    url: '../MyCSharp/BoardInteraction.asmx/GetScoreForColour',
                    data: json_data,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        var whiteScore = data.d;
                        gameOverRequestBlackScore(whiteScore);
                    },
                    failure: function (data) {
                        alert("Could not retrieve white score");
                    }
                })
            }

            // function to be called when the game finishes
            function gameOver() {
                // show the end game window
                var endGameWindow = document.getElementById("gameEndBox");
                endGameWindow.style.display = "block";

                // need to write in the two scores and who wins to the end game box
                gameOverRequestWhiteScore();
            }


            // Function to handle second ajax call when play sequence disrupted by invalid moves
            function secondAJAXWhosGo() {
                // try to see if toTry has valid moves
                var data_to_send = new Object();
                data_to_send.board = boardContents;
                data_to_send.tokenColor = whosGo;

                var json_data = JSON.stringify(data_to_send);

                $.ajax({
                    type: 'POST',
                    url: '../MyCSharp/BoardInteraction.asmx/AreAnyMovesOpenToPlayer',
                    data: json_data,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        drawBoardContents();
                        // need to make sure this is true otherwise game is over
                        if (data.d == "false") {
                            gameOver();
                        }
                    },
                    failure: function (data) {
                        alert("AreAnyMovesOpenToPlayer failed");
                    }
                })
            }
            // Function to update who's go it is; it may be the case that you can't swap as one has no moves available
            function updateWhosGo() {
                var toTry = whosGo === "white" ? "black" : "white";

                // try to see if toTry has valid moves
                var data_to_send_first_try = new Object();
                data_to_send_first_try.board = boardContents;
                data_to_send_first_try.tokenColor = toTry;

                var json_data = JSON.stringify(data_to_send_first_try);

                $.ajax({
                    type: 'POST',
                    url: '../MyCSharp/BoardInteraction.asmx/AreAnyMovesOpenToPlayer',
                    data: json_data,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data)
                    {
                        // check the rseult to see if this colour has moves
                        if (data.d == "true") {
                            // everything is fine, continue as normal
                            whosGo = toTry;
                            drawBoardContents();
                        }
                        else {
                            secondAJAXWhosGo();
                        }
                    },
                    failure: function (data)
                    {
                        alert("AreAnyMovesOpenToPlayer failed");
                    }
                })
            }
            // Function to handle a board button click
            function buttonClick(x, y)
            {
                // don't activate if game over
                if (gameIsDone === true)
                    return;

                // communicate with a web service to get the results of the click
                var dataToSend = new Object();
                dataToSend.board = boardContents;
                dataToSend.placeX = x;
                dataToSend.placeY = y;
                dataToSend.tokenColor = whosGo;

                var json_data = JSON.stringify(dataToSend);

                // use ajax to call on web service
                $.ajax({
                    type: 'POST',
                    url: '../MyCSharp/BoardInteraction.asmx/ResolvePlayerTurn',
                    data: json_data,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data)
                    {
                        if (data.d === 'invalid')
                        {
                            alert("Invalid placement location");
                        }
                        else
                        {
                            boardContents = data.d;
                            updateWhosGo();
                        }
                    },
                    failure: function (data)
                    {
                        alert("ResolvePlayerTurn failed");
                    }
                })
            }

            
            // Function to handle the user requesting the instructions
            function openInstructions() {
                if (gameIsDone === true)
                    return;

                // open the instruction page in a new tab
                window.open('../WebForms/Instructions.aspx', '_blank');
            }
        </script>

        <!-- page background -->
        <div id="backgroundImg">
        </div>

        <div id="turnIndicator">
            TURN INDICATION SHOULD GO HERE! THIS BEING LIKE THIS IS A BAD SIGN
        </div>

        <!-- A div for the end of game message box -->
        <div id="gameEndBox" style="display:none;">
            <br />
            <div id="whoWinsText" style="text-align:center; font:2em bold">
                 X WINS!
            </div>
            <br />
            <table id="finalScoreTable" style="width:60%;margin:auto;">
                <tr>
                    <th style="width:40%; font: 1.2em bold;">
                        <b>Black</b>
                    </th>
                    <th style="width:40%; font: 1.2em bold;">
                        <b>White</b>
                    </th>
                </tr>
                <tr>
                    <td style="text-align:center;">
                        Score
                    </td>
                    <td style="text-align:center;">
                        Score
                    </td>
                </tr>
            </table>
            

            <div id="gameEndMenuButton" onclick ="returnToMenu()">
                <p class="gameEndMenuButtonText">
                    <b>Return to menu</b>
                </p>
            </div>

        </div>


        <!-- The div for the table representing the board -->
        <div>
            <table id="board" style="background-color:black">
                <tr id="row0">
                    <td class="boardSpace" onclick="buttonClick(0,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,0)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
                <tr id="row1">
                    <td class="boardSpace" onclick="buttonClick(0,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,1)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
                <tr id="row2">
                    <td class="boardSpace" onclick="buttonClick(0,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,2)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
                <tr id="row3">
                    <td class="boardSpace" onclick="buttonClick(0,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,3)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
                <tr id="row4">
                    <td class="boardSpace" onclick="buttonClick(0,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,4)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
                <tr id="row5">
                    <td class="boardSpace" onclick="buttonClick(0,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,5)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
                <tr id="row6">
                    <td class="boardSpace" onclick="buttonClick(0,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,6)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
                <tr id="row7">
                    <td class="boardSpace" onclick="buttonClick(0,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(1,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(2,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(3,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(4,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(5,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(6,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                    <td class="boardSpace" onclick="buttonClick(7,7)"><img src="..\images\transparentSpace.png" class="boardSpaceImage"/></td>
                </tr>
            </table>
        </div>


        <!-- The div for the score labels -->
        <table id="scoreTable">
            <tr>
                <td class ="scoreLabel">
                    White Label
                </td>
                <td class="width:8vh"></td>
                <td class ="scoreLabel">
                    Black Label
                </td>
            </tr>
        </table>

        <div id="instructionsButton" onclick="openInstructions()">
            Instructions
        </div>
    </form>
</body>
</html>
