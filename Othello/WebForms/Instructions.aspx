﻿<!DOCTYPE html>

<html>
<head>
    <title>Othello Instructions</title>
    <meta charset="utf-8" />
    <meta name="description" content="The instructions page for Web Othello" />
    <meta name="keywords" content="othello,instructions,board,game" />
    <meta name="author" content="Jake Johnson Powell"/>
    <link rel="icon" type="image/x-icon" href="../othelloFavicon.ico" />

    <!-- Link to the CSS for this page -->
    <link href="../MyCSS/InstructionPageCSS.css" rel="stylesheet" type="text/css" />
</head>

<body>

<script>

    function backToMenu() {
        // redirect to menu
        window.location.href = "../WebForms/IntroPage.aspx";
    }

</script>

    <div id="backgroundImg">
    </div>
    

        <div id="documentPage">
            <div id="document">

                <!-- Board setup instructions -->
                Play begins with two counters on the board; 2 black, 2 white, in the centre of the board. Counters of the same colour are positioned diagonally to each other.
                <div class="instructionsSection">
                   <img  src="..\images\startBoard.png" class="instructionsImage"/>
                </div>
                <br />
                <div class="figureDesc">
                    Starting board layout
                </div>

                <br />
                <br />
                
                <!-- Basic outflanking rules -->
                On a player's move, they must place a counter of their colour, such that they 'capture' or 'outflank' a counter of their opponents
                colour. Any of the opposing player's counters that are outflanked are then replaced with counters of the same colour as the player who has just placed. 
                <div class="instructionsSection">
                   <img  src="..\images\firstMove.png" class="instructionsImage"/>
                </div>
                <br />
                <div class="figureDesc">
                    White has placed a counter which has caused a black counter to flip because it was outflanked. This could have been done diagonally.
                </div>

                
                <br />
                <br />

                <!-- No flank demonstration -->
                Counters have been outflanked when they are bordered on each end by a counter of the opposing colour, but are only flipped if one of these counters is newly placed.
                <div class="instructionsSection">
                   <img  src="..\images\secondMove.png" class="instructionsImage"/>
                </div>
                <br />
                <div class="figureDesc">
                    Black has made a move leaving the board in this state.
                </div>
                <div class="instructionsSection">
                   <img  src="..\images\thirdMove.png" class="instructionsImage"/>
                </div>
                <br />
                <div class="figureDesc">
                    White has played, leaving the board in this state; note that one white counter is flanked by two black counters, but it does not flip because neither black counter is newly placed.
                </div>

                <br />
                <br />
                
                <!-- Instruction end -->
                The winner is determined by whichever player has more counters of their colour when the game ends. The game is over when neither player can take a move; players can only
                place a counter on the board if it would result in an opposing counter being replaced, and this is not always possible. It is possible for the game to end before the 64 board spaces have been filled.
                
                <br />
                <br />
                <br />
                
                <div class="figureDesc">Tip: try to claim the corners!</div>

                <br />
                <br />

                <div id="menuButton" onclick="backToMenu()">
                    Return to menu
                </div>
         </div>
     </div>
</body>
</html>
